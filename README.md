![dali logo](https://gitlab.com/ianmethyst/dali/raw/master/img/dali.svg)

## Dotfile theming using moustache templates in pure bash.

### Description 

This script takes all the bash variables defined in two files (`$COLORSCHEME` and `$THEME`),
and replace all the placeholders that reference any of that variables enclosed by moustache 
templates inside the files that are in the `$TEMPLATE` folder, and then output the same 
folder structure to `$OUTDIR`

![Directories diagram](https://gitlab.com/ianmethyst/dali/raw/master/img/diagram.svg)

The `$THEME` file is meant to contain all the typographic and layout related variables (and
other, as required), while `$COLOR` only contain the color definitions. This enables switching
just the colorscheme or the other way around.

### Configuration

The script can be used from the command line using switches (see options secion), or it can
be configured in a bash file named `config` located at `$HOME/.config/dali`. (A default config
file is located on the root of this repository)

`$BASEDIR` is a variable that points to a directory that hold all the other revelant 
directories. All of this directories are are specified in variables in the default config 
file, and have to be on the first level of depth inside `$BASEDIR`.

All the variables set in the config file can be overwritten using switches. All the
posible variables are specified below:

| Directory      | Default value | Description                                      |
| ------------   | ------------- | ------------------------------------------------ |
| `$BASEDIR    ` | `$(pwd)`      | Directory that holds the rest of the directories |
| `$TEMPLATE   ` | `template`    | Directory with files to be parsed                |
| `$THEMES     ` | `themes`      | Themes folder                                    |
| `$COLORS     ` | `colors`      | Colorschemes folder                              |
| `$OUTDIR     ` | `out`         | Directory for outputing the parsed files         |
| `$COLORSCHEME` | `default`     | Selected colorscheme                             |
| `$THEME      ` | `default`     | Selected theme                                   |

Aditionally, two bash scripts can be specified as `$PREHOOK` and `$POSTHOOK`, that will be
run before and after the files are parsed, respectively. This allows to symlink all the files
to their right directory, for instance, as dali itself does not provide any mean to do this.

### Options 

| Switch | Description                                                          |
| ------ | -------------------------------------------------------------------- |
| `-b`   |  Set the `$BASEDIR` directory                                        |
| `-c`   |  Name of colorscheme file in `$BASEDIR/colors`                       |
| `-h`   |  Displays this message and quits                                     |
| `-l`   |  List all the themes and colorschemes available and quits            |
| `-o`   |  Set the `$OUTPUT` directory                                         |
| `-t`   |  Name of theme file in `$BASEDIR/themes`                             |
| `-s`   |  Set the directory of the settings file (default: `$SETTINGS`)       |
| `-v`   |  Print additional information when running the script                |

### Usage

#### Basic usage

To use dali just add the script to `$PATH`, and type in a teminal:

```sh
dali
```

Otherwise go to the folder where it is located and write:

```sh
./dali
```

(Make sure the script has execute permissions)

#### Specifying theme and colorscheme

To specify a theme (dejavu) and a colorscheme (solarized), issue:

```sh
dali -t dejavu -c solarized
```

### Example

A full example with a template, themes, colorschemes, and pre/post hooks can be found
on my repository: [ianmethyst's dotfiles](https://gitlab.com/ianmethyst/dotfiles)

### Additional information

To learn more about moustache templates go to:
- [Moustache](https://mustache.github.io/)
- [Mo (Moustache in bash)](https://github.com/tests-always-included/mo)
