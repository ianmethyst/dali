#!/bin/bash 

set -Eeuo pipefail
VERSION="1.0"
VERBOSE=false
ERROR=false

# Source moustache 
cd $(dirname "$0")
. "lib/mo"

# Default settings
SETTINGS=$HOME/.config/dali/config
. config.default

welcome() {
  cat <<EOF

     ·                          ·    
   :\`                            \`:  
  +                                + 
 y                                 \`y
 h                                  h
 y\`                                \`y
 .h/  ./ohmMMMNd/    /dNMMMmho/.  /h.
  \`+dNMMNmdys+:\`      \`:+sydmNMMNd+\` 
  
######################################
#         dali version: $VERSION          #
# https://gitlab.com/ianmethyst/dali #
######################################

EOF
}

help() {
  cat <<EOF

Usage: $(basename $0) [OPTION]... -t theme -c colorscheme 

Dotfile theming using moustache templates in pure bash.

This script takes all the bash variables defined in two files (`$COLORSCHEME` and `$THEME`),
and replace all the placeholders that reference any of that variables enclosed by moustache 
templates inside the files that are in the `$TEMPLATE` folder, and then output the same 
folder structure to `$OUTDIR`

To know where this variables point use -v. These can be set using switches
or by defining them in a config file (written in bash).

Options:
  -b       Set the $BASEDIR directory                                          
  -c       Name of colorscheme file in $BASEDIR/colors                         
  -h       Displays this message and quits                                     
  -l       List all the themes and colorschemes available and quits            
  -o       Set the $OUTPUT directory                                           
  -t       Name of theme file in $BASEDIR/themes                               
  -s       Set the directory of the settings file (default: $SETTINGS)         
  -v       Print additional information when running the script                

To learn more about moustache templates go to:
https://mustache.github.io/ and https://github.com/tests-always-included/mo

EOF
}

# Main function
main() {
  if [ ! "$PREHOOK" = false ]; then
    printf "\nRunning pre hook\n"
    . $PREHOOK
    printf "Finished pre hook\n"
  fi

  # Load theme and colorscheme
  . $COLORS/$COLORSCHEME
  . $THEMES/$THEME

  printf "\nParsing files \n"


  IFS=$'\n' 
  for line in $(find $TEMPLATE -type f -printf "%h%%%f\n"); do 
    # Save variables for each file
    dir=$(echo $line | cut -d % -f 1 | cut -d / -f 2-)
    file=$(echo $line | cut -d % -f 2)

    if [ "$VERBOSE" = true ]; then
      printf "Parsing $dir/$file\n"
    fi


  # Create directories in $OUTPUT
  mkdir -p $OUTDIR/$dir  
  # Parse template and output files to $OUTPUT
  cat "$TEMPLATE/$dir/$file" | mo > $OUTDIR/$dir/$file 
done

if [ ! "$POSTHOOK" = false ]; then
  printf "\nRunning post hook\n"
  . $POSTHOOK
  printf "Finished post hook\n\n"
fi

echo "Finished succesfully!"
exit 0
}

suggesthelp() {
  printf "\nTo get help use: $(basename "$0") -h (To show additional info use -v)\n"
}

dirinfo() {
  cat <<EOF

Base directory: $BASEDIR
Template directory: $TEMPLATE
Themes directory: $THEMES
Colorschemes directory: $COLORS
Output directory: $OUTDIR
EOF
}

available() {
  cat <<EOF

Available themes: $(find $THEMES -type f -printf "%f " )
Available colorschemes: $(find $COLORS -type f -printf "%f " )
EOF
}

setup() {
  if [ -r $SETTINGS ]; then
    echo "Settings found: $SETTINGS"
    . $HOME/.config/dali/config
  fi

  cd $BASEDIR
}

welcome
setup

# Parse options
while getopts "hvs:b:o:t:c:l" opt; do
  case $opt in 
    h)
      help 
      exit 0
      ;;
    o)
      OUTDIR=$OPTARG
      ;;
    b) 
      BASEDIR=$OPTARG
      ;;
    s)
      . $OPTARG
      ;;
    t) 
      THEME=$OPTARG
      ;;
    c) 
      COLORSCHEME=$OPTARG
      ;;
    v) 
      VERBOSE=true 
      echo "Running in verbose mode"
      ;;
    l)
      available
      exit 0
      ;;
    \?)
      echo "" >&2
      help
      exit 1
      ;;
  esac
done


if [ "$VERBOSE" = true ]; then
  dirinfo
fi

# Check if directories and files exist
if [ ! -d $BASEDIR ]; then
  printf "Error: \$BASEDIR ($BASEDIR) does not exist.\nThis variable should be set to an actual path\n"
  ERROR=true
fi

if [ ! -d $THEMES ]; then
  printf "Error: \$THEMES ($THEMES) does not exist.\nThis variable should be set to an actual path\n"
  ERROR=true
fi

if [ ! -d $COLORS ]; then
  printf "Error: \$COLORS ($COLORS) does not exist.\nThis variable should be set to an actual path\n"
  ERROR=true
fi

if [ ! -d $OUTDIR ]; then
  printf "Error: \$OUTDIR ($OUTDIR) does not exist.\nThis variable should be set to an actual path\n"
  ERROR=true
fi

if [ ! -r $COLORS/$COLORSCHEME ]; then
  printf "\nError: \$COLORSCHEME ($COLORSCHEME) does not exist.\nThis variable should be set to an actual file\n"
  ERROR=true
else
  if [ "$VERBOSE" = true ]; then
    echo "Using colorscheme: $COLORSCHEME"
  fi
fi

if [ ! -r $THEMES/$THEME ]; then
  printf "\nError: \$THEME ($THEME) does not exist.\nThis variable should be set to an actual file\n"
  ERROR=true
else
  if [ "$VERBOSE" = true ]; then
    echo "Using theme: $THEME"
  fi
fi

if [ "$ERROR" = true ]; then 
  suggesthelp
  exit 1
else 
  main
fi
